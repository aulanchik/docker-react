# A Simple Docker ReactJS Demo

This demo was creted for educational purposes and can be used with no limitations

## Installation

Requires:

* docker
* docker-compose

To run this container execute the following:

```
docker-compose up -d web
```

Open http://localhost:3000 and see the result
